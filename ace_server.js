'use strict';
var fs = require('fs'),
express = require('express'),
bodyParser = require('body-parser'),
cheerio = require('cheerio'),
ace = require('./modules/ace'),
ejs = require('ejs'),
app = express();

app.engine('.html', require('ejs').__express);
app.engine('.tpl', require('ejs').__express);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.set('json spaces', 0);
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
	extended: true
}));

fs.readFile('./config.json', function (err, data) {
	var config = JSON.parse(data),
	siteObj = config.sites[0];

	siteObj.currentInstance = siteObj.instances[0];

	ace.init(siteObj, function() {  
		app.get('/', function(req, res) {
			var viewData = {
				pages: [],
				ace: ace,
				config: config,
				title: ace.i18n('Administration')
			};
			
			ace.getPageNames(ace.config, function(fileNames) {
				viewData.pages = fileNames;
				return res.render('core/index.html', viewData);
			});
		});

		app.get('/page/partial/:name', function(req, res) {
			ejs.renderFile('./views/core/partials/' + req.params.name + '.html', {
				page: req.query.page,
				ace: ace,
				id: req.params.name,
				panelId: req.query.panelId
			}, function(err, template) {
				var $;
				
				if (err) {
					throw(err);
				}

				$ = cheerio.load(template);

				if (req.xhr) {
					ejs.renderFile('./views/core/partials/modalWrapper.html', {
						id: 'assignComponents',
						body: $.html(),
						header: req.query.page.name + ' - ' + req.query.panelId + ace.i18n('Panel'),
						footer: ''
					}, function(err, modalHTML) {
						res.writeHead(200, {'Content-Type': 'text/html'});
						res.write(modalHTML);
						res.end();
					});
				} else {

				}
			});
		});

		app.get('/components', function(req, res) {
			ace.getPage(req.params.pageId, function(page) {
				ace.getAllComponentsByPage(page, function(componentListing) {
					return res.render('core/listing.html', { pageComponents: componentListing });
				});
			});
		});

		app.post('/page/:pageId/panels/:panelId', function(req, res) {
			ace.getPage(req.params.pageId, function(page) {
				ace.initComponent(page, req.params.panelId, req.body.componentName, function(component) {
					ace.assignComponents(page, req.params.panelId, component, function(err, page) {
						if (err) {
							console.log(err);
						}

						ace.savePage(page, function(err, page) {
							if (err) {
								console.log(err);
							}

							return res.json(page);
						});
					})
				});
			});
		});

		// Get a particular component on a page
		app.get('/page/:pageId/panels/:panelId/components/:componentId', function(req, res) {
			
		});

		// update component on a page
		app.post('/page/:pageId/panels/:panelId/components/:componentId', function(req, res) {
			
		});

		// add component to a page
		app.post('/page/:pageId/panels/:panelId/components', function(req, res) {
			ace.getPage(req.params.pageId, function(page) {
				ace.getPanelById(req.params.panelId, function(panel) {
					ace.initComponent(page, panel, req.body.componentName, function(page, panel, component) {
						ace.assignComponents(page, panel, component, function(page, panel, component) {
							ace.savePage(page, function(err, page) {
								if (err) {
									console.log(err);
								}
								
								console.log('Added component', component);

								return res.json(page);
							});
						});
					});
				});
			});
		});

		app.delete('/page/:pageId/panels/:panelId/components/:componentId', function(req, res) {
			// callback here being fired twice 
			ace.getPage(req.params.pageId, function(page) {
				ace.removeComponent(page, req.params.componentId, function(page) {
					ace.savePage(page, function(err, page) {
						if (err) {
							console.log(err);
						}

						return res.json(page);
					});
				});
			});
		});

		app.get('/page/:fileName', function(req, res) {
			if (req.query.mode && req.query.mode !== '') {
				if (req.query.mode.toLowerCase() === 'preview') {
					//ace.createPageObject(req.params.fileName, function(pageObj) {
						ace.getPage(req.params.fileName, function(page) {
							ejs.renderFile('./views/templates/' + req.params.fileName 
								+ '.tpl', {
									page: page,
									ace: ace
							}, function(err, template) {
								var $ = cheerio.load(template);

								$('[data-ace-id]').each(function(i, item) {
									$(item).addClass('panel');
									$(item).addClass('panel-empty');
									$(item).attr('data-toggle', 'modal');
									$(item).attr('data-partial', 'assignComponents');
								});
								
								ejs.renderFile('./views/core/partials/clientScripts.html', {
									page: page,
									ace: ace
								}, function(err, footerScripts) {
									$('body').append(footerScripts);

									res.writeHead(200, {'Content-Type': 'text/html'});
									res.write($.html());
									res.end ();
								});
							});
							/*
							if (!req.xhr) {
								
							} else {
								res.json(page);
							}
							*/
						});
					//});
				}
			} else {
				// Render read mode
			}
		});

		app.post('/ace/siteSelection', function() {
			var siteName,
			site,
			siteInstance,
			i = 0;

			for (i; i < config.sites.length; i += 1) {
				site = config.sites[i];

				if (config.sites[i].id === siteName) {
					site = site;
				}
			}

			i = 0;

			if (site) {
				for (i; i < site.instances.length; i += 1) {
					if (site.instances[i] === siteInstance) {
						site.currentInstance = siteInstance;
					}
				}
			}

			ace = ace.init(site);

			return res.json({});
		});

		app.listen(8001);
	});
});
