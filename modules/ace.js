'use strict';
var fs = require('fs'),
cheerio = require('cheerio'), // DOM processing
cradle = require('cradle'), // Calls to REST page store
Ace = function() {
		
};

Ace.prototype.init = function(site, fn) {
	var ace = this;

	ace.config = site;

	ace.db = new(cradle.Connection)(ace.config.couchAddress, ace.config.couchPort, {
		auth: {
			username: ace.config.couchUser,
			password: ace.config.couchPassword
		}
	}).database(ace.config.database);

	if (typeof fn === 'function') {
		return fn(ace);
	} else {
		return ace;
	}
};

Ace.prototype.i18n = function(key, options) {
	var ace = this;

	return key;
};

Ace.prototype.getPageNames = function(site, fn) {
	fs.readdir('./views/templates', function(err, fileNames) {
		if (err) {
			throw(err);
		}

		return fn(fileNames);
	});
};

Ace.prototype.getPageData = function(pageName, fn) {
	var ace = this;

	ace.db.get(pageName, function(err, res) {
		var pageObj = {};

		if (err) {
			console.log(err);
		}

		if (!res) {
			pageObj = {
				_id: pageName,
				name: pageName,
				panels: [], // Panels contain components
				created: new Date(),
				modified: new Date(),
				author: '',
				language: '',
				instance: {},
				description: '',
				fileName: pageName + '.html', // if nothing is here then pageName.html is built out
				newPage: true,
				aceType: 'page'
			};

			return fn(err, pageObj);
		} else {
			ace.db.get(pageName, function(err, page) {
				return fn(err, page);
			});
		}
	});
};

Ace.prototype.getTemplatePanels = function(pageName, fn) {
	var ace = this;

	fs.readFile('./views/templates/' + pageName + '.tpl', 'utf-8', function(err, data) {
		var i = 0,
		$,
		panelArr = [];

		if (err) {
			throw(err);
		}

		$ = cheerio.load(data);

		ace.getComponents(function(allComponents) {
			$('[data-ace-id]').each(function(i, item) {
				var tempObj = $(item).data();

				if (typeof tempObj.components === undefined || tempObj.components === true
					|| tempObj.components === '') {
					tempObj.components = allComponents;
				} else {
					tempObj.components = tempObj.components.replace(/ /g, '').split(',');
				}

				if (typeof tempObj.componentMultiple === 'undefined' || tempObj.componentMultiple === ''
					|| tempObj.componentMultiple === true) {
					tempObj.componentMultiple = true;
				} else {
					tempObj.componentMultiple = false;
				}
				
				panelArr.push({
					id: tempObj.aceId,
					page: pageName,
					components: tempObj.components,
					multiple: tempObj.componentMultiple,
					assigned: [
						/* 
							{
								name: '',
								pageId: '',
								data: [
									{
										editHTML: '',
										readHTML: '',
										instance: ''
									}
								],
								past: null,
								aceType: 'component'
							}
						*/
					],
					data: null, // data attached in getComponentData()
					aceType: 'panel'
				});
			});

			return fn(panelArr);
		});
	});
};

// Run on a compiled page, inserts rendered components
Ace.prototype.renderPageComponents = function(pageName, componentArr, fn) {
	
};

Ace.prototype.getPageComponent = function(componentName, pageObj, fn) {
	var component = null;

	return fn(component);
};

// updating a component
// adjusts page component to reflect loaded template data
// initializes the core component module
// attaches rendered view to .html
Ace.prototype.updatePanel = function(templatePanel, pagePanel, fn) {
	var i = 0,
	ace = this;

	pagePanel.page = templatePanel.page;	
	pagePanel.panels = templatePanel.panels;
	pagePanel.multiple = templatePanel.multiple;
	
	return fn(pagePanel);
};

Ace.prototype.runComponentEvent = function(eventName, componentName, fn) {

};

// See if the pages assigned components match the new template outline
// if they do no we call updateComponent() and update the reference in the pages
// components[] property and resave. Once we know the page is correct we then process
// the valid components and attach the html for editing, display
Ace.prototype.processPanelChanges = function(pageObj, fn) {
	var ace = this,
	i = 0,
	j = 0,
	wasAlreadyDefined = false;

	// Each component on the template now should find its match within the attached components
	for (i; i < pageObj.templatePanels.length; i += 1) {
		j = 0;

		for (j; j < pageObj.panels.length; j += 1) {
			if (pageObj.templatePanels[i].id === pageObj.panels[j].id) {
				wasAlreadyDefined = true;
				// the onpage template was fx ound within the saved profile
				ace.updatePanel(pageObj.templatePanels[i], pageObj.panels[j], function(newPanel) {
					newPanel.modified = new Date();
					pageObj.panels[j] = newPanel;
				});
			}
		}

		if (!wasAlreadyDefined) {
			pageObj.panels.push(pageObj.templatePanels[i]);
		}
	}

	delete pageObj.templatePanels;

	return fn(pageObj);
};

// return a listing of all possible components
Ace.prototype.getComponents = function(fn) {
	fs.readdir('./modules/components', function(err, fileNames) {
		var i = 0;
		if (err) {
			throw(err);
		}

		for (i; i < fileNames.length; i += 1) {
			fileNames[i] = fileNames[i].replace('.js', '');
		}

		return fn(fileNames);
	});
};

Ace.prototype.savePage = function(page, fn) {
	var ace = this;

	page.modified = new Date();

	ace.db.save(page._id, page, function(err, res) {
		if (err) {
			throw(err);
		}

		return fn(err, page);
	});
};

Ace.prototype.getPage = function(pageName, fn) {
	var ace = this;
	ace.getPageData(pageName, function(err, pageObj) {
		var i = 0;
		
		if (err) {
			console.log(err);
		}

		ace.getTemplatePanels(pageName, function(panelArr) {
			pageObj.templatePanels = panelArr;
			ace.processPanelChanges(pageObj, function(pageObj) {
				ace.initComponents(pageObj.panels, function(updatedPanels) {
					pageObj.panels = updatedPanels;

					if (pageObj.newPage) {
						// if this was the first time the page was reloaded we resave.
						pageObj.newPage = false;
						ace.savePage(pageObj, function() {
							return fn(pageObj);
						});
					} else {
						return fn(pageObj);
					}
				});
			});
		});
	});
};

Ace.prototype.getTemplate= function(templateName, fn) {
	fs.readFile('./views/templates/' + req.params.pageId + '.tpl', 'utf-8', function(err, template) {
		return fn(template);
	});
};

Ace.prototype.renderPreview = function(pageObj, fn) {
	var ace = this;

	ace.getTemplatePanels(pageObj.name, function(panelArr) {
		pageObj.templatePanels = panelArr;

		ace.processPanelChanges(pageObj, function(pageObj) {
			ace.initComponents(pageObj.panels, function(updatedPanels) {
				pageObj.panels = updatedPanels;
				return fn(pageObj);
			});
		});
	});
};

// return use widget array
Ace.prototype.getTemplateWidgets = function(template, fn) {};

// Get the components description if any. Run the onAdd event
Ace.prototype.assignComponents = function(page, panel, component, fn) {
	var ace = this;
		
	if ((page.panels[i].multiple === true || (page.panels[i].multiple === false 
		&& page.panels[i].assigned.length >= 1))) {

		component.content = '';
		component.created = new Date();
		component.modified = component.created;
		component.id = page.id + '-' + panelId + '-' + component.name;
			
		page.panels[i].assigned.push(component);

		return fn(page, panel, component);
	}
};

Ace.prototype.initComponent = function(page, panel, componentName, fn) {
	var ace = this,
	componentOptions = {};
	
	require('./components/' + componentName)(ace, page, panel, componentOptions, function(component, componentEvents) {
		if (!component.name) {
			component.name = ace.i18n(componentName);
		}

		if (componentEvents.onAdd && typeof componentEvents.onAdd === 'function') {
			componentEvents.onAdd(function(canAdd, component) {
				if (canAdd) {
					return fn(page, panel, component);
				} else {
					return fn();
				}
			});
		} else {
			return fn(page, panel, component);
		}
	});
};

Ace.prototype.initComponents = function(panelArr, fn) {
	var ace = this,
	i = 0, // looping through panels
	j = 0, // Looping through panel components
	component; // Reference to the assigned component we're working on with j

	for (i; i < panelArr.length; i += 1) {
		j = 0;
		if (panelArr[i].components && panelArr[i].components.length !== 0) {
			if (panelArr[i].assigned.length > 0) {
			//	for (j; j < panelArr[i].assigned.length; i += 1) {
					//component = panelArr[i].assigned[j];
					//component.data = require('./components/' + component.name)(app, component, panelArr[i]);
			//	}
			}
		}
	}

	return fn(panelArr);
};

Ace.prototype.getPanelById = function(page, panelId, fn) {
	var i = 0,
	panel;

	for (i; i < page.panels.length; i += 1) {
		if (page.panels[i].id === panelId) {
			panel = page.panels[i];
		}
	}
	
	return fn(panel);
};

// returns an array of a pages assigned components, across all panels
Ace.prototype.getAllComponentsByPage = function(page, fn) {
	var i = 0,
	j = 0,
	componentListing = [];


	for (i; i < panelArr.length; i += 1) {
		j = 0;
		if (panelArr[i].assigned) {
			for (j; j < panelArr[i].assigned.length; i += 1) {
				componentListing.push(panelArr[i].assigned[j]);
			}
		}
	}

	return fn(componentListing);
};

// returns an array of all of a panels assigned components
Ace.prototype.getAllComponentsByPanel = function(page, panelId, fn) {

};

// Order a panels assigned components by order #
Ace.prototype.orderPanelComponents = function(page, panelId, fn) {

};

module.exports = new Ace();
