/*
* Standard blockquote element creation
*/
module.exports = function(ace, page, panelId, options, fn) {
	'use strict';

	var model = {
		description: ace.i18n('Use this component to insert a quotation or highlight block.'),
	},
	events = {
		onAdd: function(callback) {
			return callback(true, model);
		}
	};

	return fn(model, events);
};
