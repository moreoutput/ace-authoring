/*
* Page Meta Information
*/
module.exports = function(app, componentObj, panelObj) {
	console.log('Initializing the List Component');

	this.elementType = 'ul';
	this.elementProperties = '';
	this.listItems = [/*
		{
			url:
			value:
			className:
			id: 
			target: 
		}
	*/];
	this.classes = '';
	this.created = new Date();

	return this;
};
