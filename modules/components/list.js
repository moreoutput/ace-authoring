/*
* Insert HTML list.
*/
module.exports = function(ace, page, panelId, componentName, data, fn) {
	var component = {},
	callback;

	if (typeof data === 'function') {
		callback = data;
	} else {
		callback = fn;
	}

	component.panelId = panelId;
	componentpageId = page._id;
	component.created = new Date();
	component.modified = new Date();
	component.aceType = 'component';
	component.mode = 'read';
	component.order = 0;
	component.data = data;
	component.name = ace.i18n('List Creation');
	component.description = ace.i18n('HTML List Element Component');
	component.elementType = 'ul';
	component.elementProperties = '';
	component.listItems = [];
	component.classes = '';

	component.onModeSwitch = function() {

	}

	component.onRead = function() {
		
	}

	// Must return true for component to be added;
	component.onAdd = function(fn) {
		return fn(true);
	};

	// On save needs to return the components data object built from 
	// the passed in and gathered data.
	component.onSave = function() {

	};

	component.onUpdate = function() {

	};

	component.onRemove = function() {

	};

	component.onMoreThanOne = function() {

	};

	console.log('HELLO WORLD');

	if (typeof callback === 'function') {
		return callback(component);
	} else {
		return component;
	}
};
