/*
* Insert HTML list.
*/
module.exports = function(ace, page, panelId, componentName, data, fn) {
	var component = {},
	callback;

	if (arguments.length === 4 && typeof data === 'function') {
		callback = data;
	} else {
		callback = fn;
	}
	
	component.panelId = panelId;
	componentpageId = page._id;
	component.created = new Date();
	component.modified = new Date();
	component.aceType = 'component';
	component.mode = 'read';
	component.data = data;
	component.name = ace.list('Header Creation');
	component.description = ace.list('HTML Header Component');
	component.elementType = 'ul';
	component.elementProperties = '';
	component.listItems = [
	/*
		{
			url:
			value:
			className:
			id:
			target:
		}
	*/
	];

	component.classes = '';

	component.onModeSwitch = function() {
		console.log('Mode Switch');
	}

	component.onRead = function() {
		console.log('Read Mode');
	}

	// Must return true for component to be added;
	component.onAdd = function(fn) {
		console.log('On Add');
		return fn(true);
	};

	// On save needs to return the components data object built from 
	// the passed in and gathered data.
	component.onSave = function() {

	};

	component.onUpdate = function() {

	};

	component.onRemove = function() {

	};

	component.onMoreThanOne = function() {

	};

	if (typeof callback === 'function') {
		return callback(component);
	} else {
		return component;
	}
};
