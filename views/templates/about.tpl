<% include header.tpl %>
	<h1><%= ace.i18n('About Page') %></h1>
	<!-- One of any component -->
	<div class="col-md-12" data-ace-id="about-header" data-components="true" data-component-multiple="false"></div>
	<!-- Author only with listed components, but as many times as we want -->
	<div class="col-md-6" data-ace-id="about-content" data-components="rte, quote" data-component-multiple="true"></div>
	<!-- Just list components in the sidebar panel -->
	<div class="col-md-6" data-ace-id="about-sidebar" data-components="list, header"></div>
<% include footer.tpl %>