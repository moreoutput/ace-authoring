<!doctype html>
<html>
	<head>
		<title>
			<%= ace.config.name %>
		</title>
		<link rel="stylesheet" href="/css/core/bootstrap.min.css" />  
		<link rel="stylesheet" href="/css/core/bootstrap-theme.min.css" />
		<link rel="stylesheet" href="/css/core/styles.css" />
	</head>
	<body>
		<div id="wrapper" class="container l-pad">
			<div class="row">