#Ace Authoring#

##0.0.1##

Ace is a CMS for developing authoring experiences for web content. Ace is not concerned with user access, being a DAM, or serving pages.

The end result of an authoring experience is a .html file.

Ace Authoring is an express.js 4.x application with multi-site and i18n support.

The Ace server does expect some notion of user privilege. Options being reader (only views pages), editor (edits pages), and admin (full access). However the server does not manage any users directly. A call or check should be written in ace.client.js to authorize; and a user object built and passed into ace authoring. The current username and authoring role are passed to the server on each call. By default the application treats everyone as an administrator.

##Site Development##

###What is a Template (.tpl) and how do I create them?###
Templates represent the core structure of a page you hope to build. Defining a new .tpl file in the templates directory will allow the page to be edited across websites; to limit page access to a particular site create a 1:1 copy of your sites build out path within views/templates.

For example: 

	* /views/templates/about.tpl will register as a template across all sites. 
	* /views/templates/example.com/about.tpl will register as a template to example.com.
	* /views/templates/example.com/en/about.tpl will register as a page across example.com/en

A full site directory example; in which the index pages of each country are different templates:

	* /ace/views/templates/index.tpl (root index acts as the default index template if nothing exists in a child directory)
	* /ace/views/templates/example.com/about.html
	* /ace/views/templates/example.com/uk_en/
	* /ace/views/templates/example.com/ca_en/index.tpl
	* /ace/views/templates/example.com/us_en/index.tpl
	* /ace/views/templates/example.com/us_en/about.tpl

When the above compiles the following is created in ace/sites:

	* /example.com/uk_en/index.html (default index template)
	* /example.com/uk_en/about.html
	* /example.com/ca_en/index.html
	* /example.com/ca_en/about.html
	* /example.com/us_en/index.html
	* /example.com/us_en/about.html

Templates should be valid EJS or HTML files.

###Page Modes and Component Testing###
Templates have a number of authoring views, you can turn any particular mode off in config.json:

    * /page/:filename/preview is preview mode
    * /page/:filename/edit is edit mode
    * /page/:filename/notes is anotation mode (users can highlight and share notes about content, marking them open and closed)
    * /page/:filename/design for adding and editing assigned components.

###What is a Panel?###
Panels are outlined areas on your template. Think of them as your content areas which have components assigned by an author.
* Reach any panel in design mode with: /page/pageName/panelID/design

###What is a Component (.cmp)?###
A component is the display and creation logic for a section of DOM data. 
	
	* All components switch modes with the page.
	* Reach any component in preview mode via /page/pageName/panelID/componentID/preview
	* Reach any component in edit mode via /page/pageName/panelID/componenID/edit 

####Component Development####

	*.js file added to modules/components/<component name>
	* component .js file controls addtional logic and requests related to this component
	* anything returned from the component can be found in a view under the component name. Like page.components[NAME].body

Minimum views to define in /views/components/

	* read.cmp - display template
	* edit.cmp - edit template

###Localization###
Nomarks localization module will support JSON with an XLIFF fallback for site localization. 

It will check for a static JSON file locally if that fails it will look for an XLIFF. You can define priorty and/or turn off the xliff check in
config.json.

```js
/*
 Pass in a key to match against, and a possible fallback string. 
 If no fallback string is provided and the key fails to match the key will be displayed.
*/
nomark.i18n('key', 'fallbackString');
```
