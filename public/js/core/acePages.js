/*
* Namespace for all functions concerning Ace pages.
*/
define(['jquery'], function($, Panels) {	
	var AcePage = function(site) {
		this.profile;
		this.components = [];
		this.panels = [];
	};

	AcePage.prototype.init = function(fn) {
		return fn(page);
	};
	
	return new AcePage();
});
