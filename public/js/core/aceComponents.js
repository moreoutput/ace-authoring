define(['jquery'], function($) {
	var Component = function() {

	};

	Component.prototype.get = function(page, panelId, componentName, fn) {
		var cmp = this;
		return fn({	name: componentName });
	};

	Component.prototype.assign = function(page, panelId, componentName, fn) {
		var cmp = this;

		$.ajax({
			url: '/page/' + cmp.page._id + '/panels/' + panelId + '/components',
			type: 'post',
			data: JSON.stringify({
				pageId: cmp.page._id,
				componentName: componentName
			}),
			contentType: 'application/json',
			success: function(res) {
				cmp.page = res;

				if (res.aceType) {
					$('#assignComponents')
					.find('.component-listing')
					.find('ul')
					.append('<li><h3>' + res.name + '</h3>' 
						+ '<p>' + res.description + '</p>'
						+ '<button class="btn btn-primary">Remove Component</button></li>');

					return fn();
				}
			}
		});
	};
	
	return new Component();
});	
