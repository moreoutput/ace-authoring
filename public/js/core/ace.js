/*
* This module namespaces all functions related to working with Ace
*/
define(['jquery'], function($) {
	'use strict';
	var Ace = function() {
		this.user;
		this.site;
	};

	Ace.prototype.init = function(pageId, fn) {
		var ace = this;

		$.get('/', function(site) {
			$.get('/page/' + pageId, function(res) {
				return fn(res.body);
			});
		});
	};

	Ace.prototype.getParams = function() {

	};
	
	return new Panel();
});
