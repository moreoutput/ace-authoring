/**
* This module sets up page events and is the entry point for the ace client
**/
define(['jquery', 'core/acePages', 'bootstrap'], function($, Ace, Page, Panel, Component) {
	'use strict';

	// Functions related to DOM event setup
	var currentPage,
	setupPanelEvents = function(fn) {
		var setupModal = function(item) {
			$(item).on('click', function(e) {
				e.preventDefault();

				$.ajax({
					url: '/page/partial/' + $(item).data('partial'),
					type: 'get',
					data: {
						page: ace.page,
						panelId: $(item).data('ace-id')
					},
					contentType: 'application/json',
					dataType: 'html',
					success: function(modalHTML) {
						if ($('#' + $(item).data('partial')).length !== 0) {
							$('#' + $(item).data('partial')).remove();
						}
						
						$('body').append(modalHTML);
						
						$('#'+ $(item).data('partial') + '-submit').on('click', function(e) {
							e.preventDefault();

							Component.assign(ace.page, $(item).data('ace-id'),
								$('[name=componentSelection]').val(), function(wasAssigned) {
								if (wasAssigned) {
									console.log('Component assigned');
								} else {
									console.log('Component was not assigned');
								}
							});
						});

						$('#' + $(item).data('partial')).modal();

						return fn();
					}
				});
			});
		};

		Ace.init(function(siteProfile, pageProfile) {
			Page.profile = pageProfile;
	
			// Array of IDs
			Page.components = Components.getAll(page);
			Page.panels = Panels.getAll();

			// Calling the DOM setup functions outlined above
			$('[data-toggle=modal]').each(function(i, item) {	
				setupModal(item);
			});
		});
	}
});
